#include <Wire.h>
#include "IRremote.h"
#include "RTClib.h"
#include "remote.h"
#include <Time.h>

RTC_DS1307 RTC;

void AdjustTimeToCompileTime()
{
  char* months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
  char *date = __DATE__;
  char *tim = __TIME__;
  int mon;
  for (mon = 1; mon <= 12 && strncmp(date, months[mon - 1], 3) != 0; mon++);
  DateTime cdate(atoi(date + 7), mon, atoi(date + 4), atoi(tim), atoi(tim + 3), atoi(tim + 6));
  //PrintDate(cdate);
  DateTime add5sec(cdate.unixtime() + 5);
  RTC.adjust(add5sec);
}

void setup () {
  Serial.begin(9600);
  Wire.begin();
  RTC.begin();
  if (RTC.isrunning() == 0)
  {
    Serial.print("Update time to compile time\r\n");
    AdjustTimeToCompileTime();
  }
  else
  {
    Serial.print("Current time is ");
    ShowCurrentTime();
  }
}

DateTime last;

void loop()
{
  DateTime cur = RTC.now();
  if (cur.unixtime() != last.unixtime())
  {
    if (cur.hour() >= 10 && cur.hour() <= 18 && cur.second() == 0 && cur.minute() == 0) // Every hour from 10:00 to 18:00
      //if(cur.hour()==16 && cur.second()==0 && cur.minute()==0)  // Every day in 16:00
      //if(cur.second()==0 && cur.minute()==0)  // Every hour
      //if(cur.second()==0 && (cur.minute()%30==0))  // Every 30 minutes
      //if(cur.second()==0 && (cur.minute()%10==0))  // Every 10 minutes
      //if(cur.second()==0)  // Every minute
      //if((cur.second()%30==0))  // Every 30 seconds
      //if(cur.second()%10==0) // Every 10 seconds
    {
      ShowCurrentTime();
      SendOlympus(OLY_SHOOT);
    }
    last = cur;
  }
  delay(500);
}

void PrintDate(DateTime date)
{
  // Выводим время в монитор порта
  Serial.print(date.year(), DEC);
  Serial.print('/');
  Serial.print(date.month(), DEC);
  Serial.print('/');
  Serial.print(date.day(), DEC);
  Serial.print(' ');
  Serial.print(date.hour(), DEC);
  Serial.print(':');
  Serial.print(date.minute(), DEC);
  Serial.print(':');
  Serial.print(date.second(), DEC);
  Serial.println();
}

void ShowCurrentTime() {
  // Определяем время
  PrintDate(RTC.now());
}

