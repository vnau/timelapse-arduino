﻿# -*- coding: utf-8 -*-
import glob, os, time, shutil
import exifread
import sys
from datetime import datetime
title_template = '{%d}{%d} ЖСК "Дом ученых" %s\n'
shoots_path = 'shoots'

def get_file_date(filename):
	# Open image file for reading (binary mode)
	with open(filename, 'rb') as f:
		# Return Exif tags
		tags = exifread.process_file(f)
		strtime = str(tags['EXIF DateTimeOriginal'])
		return datetime.strptime(strtime, "%Y:%m:%d %H:%M:%S")

if not os.path.exists(shoots_path):
	print('No "%s" folder with timelapse shoots found'% shoots_path)
	sys.exit(-1)
		
# Make hourly dirs
for hour in range(5,23):
	index = 1
	print("Hour %02d" % hour)
	path = "%02d" % hour
	if not os.path.exists(path):
		os.makedirs(path)
	descript = open('%s\\descript.ion' % path, 'w', encoding='cp1251')
	titles = open('%s\\title.sub' % path, 'w', encoding='cp1251')
	for file in glob.glob(shoots_path+'\\*.jpg'):
		#tim = time.localtime(os.path.getmtime(file));
		tim = get_file_date(file)
		if (tim.hour==hour and tim.minute<20) or (tim.hour==hour-1 and tim.minute>40):
			#timestring = time.strftime("%Y %b %d", tim)
			timestring = '{0:%Y %b %d}'.format(tim)
			print("%s %s"% (file,timestring))
			newfilename = "%03d.jpg" % index
			shutil.copy2(file, "%s\%s"%(path , newfilename))
			descript.write("%s %s %s\n" % (newfilename, file, timestring))
			titles.write(title_template % (index-1, index, timestring))
			index = index  + 1
	descript.close()
	titles.close()
			
# make allday
index = 1
path = "allday"
print("All day")
if not os.path.exists(path):
	os.makedirs(path)
descript = open("%s\\descript.ion" % path, "w", encoding="cp1251")
titles = open("%s\\titles.sub" % path, "w", encoding="cp1251")
for file in glob.glob(shoots_path+'\\*.jpg'):
	tim = get_file_date(file)
	if tim.hour>=5 and tim.hour<=20:
		timestring = '{0:%Y %b %d}'.format(tim)
		print("%s %s"% (file,timestring))
		newfilename = "%03d.jpg" % index
		shutil.copy2(file, "%s\%s"%(path, newfilename))
		descript.write("%s %s %s\n" % (newfilename, file, timestring))
		titles.write(title_template % (index-1, index, timestring))
		index = index  + 1
descript.close()
titles.close()