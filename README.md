### Timelapse timer with IR remote based on Arduino ###

Could be used for long-term timelapse. For example to make shoot once per day.

Hardware configuration:

* Arduino UNO board.
* DS1307 Real-time clock module. 
* IR transmitter led.

Timelapse controller features:

* Ability to make shoots every day in the same time to get pictures with similar shadow phase. Supposed that it will give us smoother timelapse video.
* Continue shooting in specified time after power failure.
* Control the camera using IR remote control as it has no other way to trigger a shutter.
* Set the controller's current date to sketch compile time if it was not initialized early.

The shooting period was hard-coded in source code as it was no reason to change it for long-term timelapse. The DS1307 module has an EEPROM memory so you could modufy code to store current shooting period in EEPROM.